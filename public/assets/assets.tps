<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.12.1</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser</string>
        <key>textureFileName</key>
        <filename>assets.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantLow</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png8</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Grid</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>json</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>assets.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Blocks/block_01.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Blocks/block_02.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Blocks/block_03.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Blocks/block_04.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Blocks/block_05.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Blocks/block_06.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Blocks/block_07.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Blocks/block_08.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_01.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_02.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_03.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_04.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_05.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_06.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_07.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_08.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_09.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_10.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_11.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_12.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_13.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_14.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_15.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_16.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_17.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_18.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_19.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_20.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_21.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_22.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_23.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_24.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_25.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_26.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_27.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_28.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_29.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_30.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_31.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_32.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_33.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_34.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_35.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_36.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_37.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_38.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_39.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_40.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_41.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_42.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_43.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_44.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Crates/crate_45.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Environment/environment_01.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Environment/environment_02.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Environment/environment_03.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Environment/environment_04.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Environment/environment_05.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Environment/environment_06.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Environment/environment_07.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Environment/environment_08.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Environment/environment_09.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Environment/environment_10.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Environment/environment_11.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Environment/environment_12.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Environment/environment_13.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Environment/environment_14.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Environment/environment_15.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Environment/environment_16.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Ground/ground_01.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Ground/ground_02.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Ground/ground_03.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Ground/ground_04.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Ground/ground_05.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Ground/ground_06.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/Dark/playerFace_dark.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/Dark/player_01.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/Dark/player_02.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/Dark/player_03.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/Dark/player_04.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/Dark/player_05.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/Dark/player_06.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/Dark/player_07.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/Dark/player_08.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/Dark/player_09.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/Dark/player_10.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/Dark/player_11.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/Dark/player_12.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/playerFace.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/playerFace_outline.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/player_01.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/player_02.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/player_03.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/player_04.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/player_05.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/player_06.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/player_07.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/player_08.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/player_09.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/player_10.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/player_11.png</key>
            <key type="filename">../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game/Player/player_12.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../../../Google Drive/tutorials/phaser/crazygames.com/kenney_sokobanPack/game</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
