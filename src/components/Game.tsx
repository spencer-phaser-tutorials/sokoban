import 'phaser';
import * as React  from 'react';
import Main from '../Scenes/Main';

export const config = {
    backgroundColor: "#ffffff",
    height: 640,
    parent: 'game',
    physics: {
        arcade: {
            gravity: { x: 0, y: 0 },
        },
        default: 'arcade',
    },
    pixelArt: true,
    roundPixels: true,
    scene: Main,
    title: 'Sokoban',
    type: Phaser.WEBGL,
    width: 480,
}


export interface IProps {
    game: Phaser.Game | null;
    initializeGame: (game: Phaser.Game) => void;
}

export default class Game extends React.Component<IProps, {}> {
    public render(){
        return <div id="game" />;
    }
    public componentWillMount(){
        this.props.initializeGame(new Phaser.Game(config));
    }
}