import 'phaser';
import * as React from 'react';
import './App.css';
import Game from "./components/Game";

export interface IState {
    game: Phaser.Game | null
}

class App extends React.Component<{}, IState> {
    constructor(props: any){
        super(props)
        this.state = { game: null };
        this.initializeGame = this.initializeGame.bind(this);
    }
    public initializeGame(game: Phaser.Game){
        this.setState({ game })
    }

    public render() {
        return (
            <div className="App">
                <h1>{this.state.game ? this.state.game.config.gameTitle : ''}</h1>
                <Game
                    game={this.state.game}
                    initializeGame={this.initializeGame}
                />
            </div>
        );
    }
}

export default App;
