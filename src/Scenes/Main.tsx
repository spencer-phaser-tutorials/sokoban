import 'phaser';

const MAP_WIDTH = 5;

export default class Main extends Phaser.Scene{
    constructor(){
        super("Main")
    }
    public preload(): void {
        this.load.atlas(
            'assets',
            './assets/assets.png',
            './assets/assets.json'
        );
        this.load.tilemapTiledJSON(
            "level01",
            "assets/levels/level01.json"
        );
    }
    public create(): void {
        const { width, height } = this.game.config;

        const map = this.make.tilemap({ key: "level01" });
        const tileset = map.addTilesetImage("assets", "assets");

        const x = (+width / 2) - (tileset.tileWidth * MAP_WIDTH / 2);
        const y = (+height / 8);

        // @ts-ignore
        const groundLayer = map.createStaticLayer("Ground", tileset, x, y);
        // @ts-ignore
        const boxesLayer = map.createStaticLayer("Boxes", tileset, x, y);
    }

    // @ts-ignore
    private greeting(){
        const { width, height } = this.game.config;
        this.add.text(
            +width / 2,
            +height / 2,
            'Hey there'
        ).setOrigin(0.5);
    }
}
