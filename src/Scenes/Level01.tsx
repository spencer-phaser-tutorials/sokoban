import * as Phaser from 'phaser';
import Crate from '../Sprites/Crate';
import Player from '../Sprites/Player';

class LevelScene extends Phaser.Scene {

    private player: Player;
    private crates!: Phaser.GameObjects.Group;

    // tilemap
    private tileSet: Phaser.Tilemaps.Tileset;
    private tileMap: Phaser.Tilemaps.Tilemap;

    // layers
    private floorLayer: Phaser.Tilemaps.StaticTilemapLayer;
    private wallLayer: Phaser.Tilemaps.StaticTilemapLayer;
    private spawnLayer: Phaser.Tilemaps.StaticTilemapLayer;
    private goalLayer: Phaser.Tilemaps.StaticTilemapLayer;
    private crateLayer: Phaser.Tilemaps.StaticTilemapLayer;

    constructor() {
        super({
            key: 'LevelScene',
        });
    }

    public preload() {
        this.load.tilemapTiledJSON('level01', `./assets/levels/level01.json`);
    }

    public create() {
        this.tileMap = this.make.tilemap({ key: 'level01' });
        this.tileSet = this.tileMap.addTilesetImage('assets');
        this.createLevel();
        this.createPlayer();
    }

    private createLevel() {
        this.createLayers();
        this.createCrates();
    }

    private createLayers() {
        const x = 0;
        const y = 0;
        this.spawnLayer = this.tileMap.createStaticLayer('Spawn', this.tileSet, x, y);
        this.spawnLayer.setVisible(false);
        this.crateLayer = this.tileMap.createStaticLayer('Crates', this.tileSet, x, y);
        this.crateLayer.setVisible(false);
        this.floorLayer = this.tileMap.createStaticLayer('Floors', this.tileSet, x, y);
        this.wallLayer = this.tileMap.createStaticLayer('Walls', this.tileSet, x, y);
        this.goalLayer = this.tileMap.createStaticLayer('Goals', this.tileSet, x, y);
    }

    private createCrates() {
        const crateTiles = this.getTiles((tile) => {
            return tile.index > -1;
        }, this.crateLayer);
        const crateSprites = crateTiles.map((tile) => {
            const { x, y } = this.tileMap.tileToWorldXY(tile.x, tile.y);
            const { type } = tile.properties as { type: number };
            const crate = new Crate(this, x, y, type);
            this.add.existing(crate);
            return crate;
        });
        this.crates = this.add.group(crateSprites);
    }

    private createPlayer() {
        const playerSpawn = this.getSpawn();
        const { x, y } = this.tileMap.tileToWorldXY(playerSpawn.x, playerSpawn.y);
        this.player = new Player(this, x, y);
        this.add.existing(this.player);
    }
}

export default LevelScene;