import * as Phaser from 'phaser';

const START_FRAME = 'Crates/crate_12'; // depends on your tilesheet, check out the JSON

class Crate extends Phaser.GameObjects.Sprite {
    constructor(scene: Phaser.Scene, x: number, y: number, private crateType: number) {
        super(scene, x, y, 'assets', crateTypeToAsset(crateType));
    }
}

export default Crate;